﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;
namespace BookTour.Wasm.Services
{
    public class TokenService
    {
        private readonly AuthenticationStateProvider _authenticationStateProvider;

        public TokenService(AuthenticationStateProvider authenticationStateProvider)
        {
            _authenticationStateProvider = authenticationStateProvider;
        }

        public async Task<string> GetTokenAsync()
        {
            var authState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            var user = authState.User;

            if (user.Identity.IsAuthenticated)
            {
                var jwtTokenClaim = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Authentication);
                if (jwtTokenClaim != null)
                {
                    return jwtTokenClaim.Value;
                }
            }

            return string.Empty;
        }
    }
}
