﻿namespace BookTour.Wasm.IServices
{
    public interface ITokenProvider
    {
        void SetToken(string token);    
        string? GetToken();
        void ClearToken();
    }
}
