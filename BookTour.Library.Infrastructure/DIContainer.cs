﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Infrastructure.ApplicationContext;
using BookTour.Library.Infrastructure.Mapper;
using BookTour.Library.Infrastructure.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace BookTour.Library.Infrastructure
{
    public static class DIContainer
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddDbContext<BookTourContext>();
            IMapper mapper = MappingConfig.RegisterMaps().CreateMapper();
            services.AddSingleton(mapper);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IImageRepository, ImageRepository>();
            services.AddScoped<ITourRepository, TourRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IBookingRepository, BookingRepository>();
            return services;
        }
    }
}
