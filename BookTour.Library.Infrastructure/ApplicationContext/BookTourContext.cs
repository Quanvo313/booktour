﻿using BookTour.Library.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BookTour.Library.Infrastructure.ApplicationContext
{
    public class BookTourContext : DbContext
    {
        public BookTourContext()
        {
        }

        public DbSet<CompanyDetail> CompanyDetails { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<TourType> TourTypes { get; set; }
        public DbSet<Transportation> Transportations { get; set; }
        public DbSet<Payment> Payments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string jsonFileName = "connection.json";

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .AddJsonFile(jsonFileName)
                .Build();

            IConfigurationSection connectionStringSection = configuration.GetSection("BookTour");
            string connectionString = connectionStringSection.Value;
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TourType>().HasData(
                 new TourType { TourTypeId = 1, TourTypeName = "Adventure" },
                 new TourType { TourTypeId = 2, TourTypeName = "Relaxation" }
             );

            modelBuilder.Entity<Transportation>().HasData(
                new Transportation { TransportationId = 1, TransportationName = "Bus" },
                new Transportation { TransportationId = 2, TransportationName = "Plane" },
                new Transportation { TransportationId = 3, TransportationName = "Car" }
            );



            modelBuilder.Entity<Tour>().HasData(
                new Tour
                {
                    TourId = 1,
                    Start = "Hồ Chí Minh",
                    TourName = "Amazing Adventure",
                    TourTypeId = 2,
                    Description = "An exciting adventure tour.",
                    PricePerPerson = 500M,
                    TourDuration = 5,
                    TransportationId = 1,
                    Destination = "Hoa Binh",
                    StartDate = new DateTime(2023, 1, 1),
                    EndDate = new DateTime(2023, 1, 6)
                },
                 new Tour
                 {
                     TourId = 2,
                     Start = "Hồ Chí Minh",
                     TourName = "Chuyến đi đáng nhớ",
                     TourTypeId = 1,
                     Description = "An exciting adventure tour.",
                     PricePerPerson = 500M,
                     TourDuration = 5,
                     TransportationId = 1,
                     Destination = "Hà Nội",
                     StartDate = new DateTime(2024, 3, 2),
                     EndDate = new DateTime(2024, 10, 2)
                 },
                 new Tour
                 {
                     TourId = 3,
                     Start = "Hồ Chí Minh",
                     TourName = "Miền tây mãi đỉnh",
                     TourTypeId = 1,
                     Description = "Đi về miền tây.",
                     PricePerPerson = 500000M,
                     TourDuration = 3,
                     TransportationId = 1,
                     Destination = "Cần Thơ",
                     StartDate = new DateTime(2024, 3, 2),
                     EndDate = new DateTime(2024, 3, 5)
                 },
                 new Tour
                 {
                     TourId = 4,
                     Start = "Hồ Chí Minh",
                     TourName = "Miền tây mãi đỉnh phần 2",
                     TourTypeId = 1,
                     Description = "Đi về miền tây.",
                     PricePerPerson = 500000M,
                     TourDuration = 3,
                     TransportationId = 1,
                     Destination = "Hậu Giang",
                     StartDate = new DateTime(2024, 3, 2),
                     EndDate = new DateTime(2024, 3, 5)
                 },
                 new Tour
                 {
                     TourId = 5,
                     Start = "Hồ Chí Minh",
                     TourName = "Đi tìm cái lạnh",
                     TourTypeId = 1,
                     Description = "Đi Đà Lạt vài ngày.",
                     PricePerPerson = 500000M,
                     TourDuration = 3,
                     TransportationId = 1,
                     Destination = "Đà Lạt",
                     StartDate = new DateTime(2024, 3, 2),
                     EndDate = new DateTime(2024, 3, 5)
                 }
            );

            modelBuilder.Entity<Booking>().HasData(
                new Booking
                {
                    BookingId = 1,
                    TourId = 1,
                    UserId = "0c8c5fb5-b984-4344-b077-d660ac3f45f4",
                    NumberOfPeople = 2,
                    BookingDate = DateTime.Now,
                    TotalCost = 1000M
                }
            );

            modelBuilder.Entity<Payment>().HasData(
                new Payment
                {
                    PaymentID = 1,
                    BookingID = 1,
                    PaymentDate = DateTime.Now,
                    PaymentMethod = "Credit Card",
                    Amount = 1000M,
                    IsCompleted = true,
                }
            );

            modelBuilder.Entity<Image>().HasData(
                new Image
                {
                    ImageId = 1,
                    ImageUrl = "https://vcdn1-dulich.vnecdn.net/2022/05/13/hohoabinh-1652415316-5109-1652415499.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=8KCD1P1Rg6b2V-MINSu3Bw",
                    ImageText = "A beautiful view",
                    IsMain = true,
                    TourId = 1
                },
                new Image
                {
                    ImageId = 2,
                    ImageUrl = "https://vukehoach.mard.gov.vn/atlas/prov/hoabinh/hoabinh.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 1
                },
                new Image
                {
                    ImageId = 3,
                    ImageUrl = "https://lh3.googleusercontent.com/BIiO6AXgkD42Pv26utjmStCmUULp2T98xTDX4W1sZyaaBUF0B21cy-i2O9hphR9LrTzSdMCqSayrXOT1MGes8lz2n1v_5Kok98MBMbKa2u10Vxb89sBXNRrOu7XJhhkhY6tOsy6pTk6T1Qki1aQ8EwGbwaOFLYf0rUSsad4J73lzn7Rqj-GEnWA_jqkbIg9acXJjmYlkPQ",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 1
                },
                new Image
                {
                    ImageId = 4,
                    ImageUrl = "https://bcp.cdnchinhphu.vn/334894974524682240/2023/2/7/hn-1675770617088584662218.jpg",
                    ImageText = "A beautiful view",
                    IsMain = true,
                    TourId = 2
                },
                new Image
                {
                    ImageId = 5,
                    ImageUrl = "https://statics.vinpearl.com/du-lich-ha-noi-4-ngay-3-dem-1_1681407076.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 2
                },
                new Image
                {
                    ImageId = 6,
                    ImageUrl = "https://thanhnien.mediacdn.vn/Uploaded/tiendung/2022_09_21/78b0bcc5-d37b-4301-8ee3-679d35df3c17-2002.jpeg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 2
                },
                new Image
                {
                    ImageId = 7,
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/c/c4/Can-tho-tuonglamphotos.jpg",
                    ImageText = "A beautiful view",
                    IsMain = true,
                    TourId = 3
                },
                new Image
                {
                    ImageId = 8,
                    ImageUrl = "https://ik.imagekit.io/tvlk/blog/2022/09/kinh-nghiem-du-lich-can-tho-1.jpg?tr=dpr-2,w-675",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 3
                },
                new Image
                {
                    ImageId = 9,
                    ImageUrl = "https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2022/12/2/1123288/Can-Tho-1.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 3
                },
                new Image
                {
                    ImageId = 10,
                    ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/2/24/Th%C3%A1p_%C4%90%E1%BB%93ng_H%E1%BB%93_%E1%BB%9F_V%E1%BB%8B_Thanh.jpg",
                    ImageText = "A beautiful view",
                    IsMain = true,
                    TourId = 4
                },
                new Image
                {
                    ImageId = 11,
                    ImageUrl = "https://vcdn-vnexpress.vnecdn.net/2023/10/16/b09b04cdd9840eda5795-3735-1697439022.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 4
                },
                new Image
                {
                    ImageId = 12,
                    ImageUrl = "https://thanhnien.mediacdn.vn/Uploaded/dieutrang-qc/2022_07_04/vi-thanh-hau-giang-1-1238.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 4
                },
                new Image
                {
                    ImageId = 13,
                    ImageUrl = "https://ik.imagekit.io/tvlk/blog/2023/01/canh-dep-da-lat-1.jpg?tr=dpr-2,w-675",
                    ImageText = "A beautiful view",
                    IsMain = true,
                    TourId = 5
                },
                new Image
                {
                    ImageId = 14,
                    ImageUrl = "https://static.vinwonders.com/production/da-lat-duoc-menh-danh-la-gi-top-banner.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 5
                },
                new Image
                {
                    ImageId = 15,
                    ImageUrl = "https://statics.vinpearl.com/canh-dep-da-lat-1_1688379739.jpg",
                    ImageText = "A beautiful view",
                    IsMain = false,
                    TourId = 5
                }
            );

        }
    }
}
