﻿using BookTour.Library.Application.IRepository;
using BookTour.Library.Domain.Entities;
using BookTour.Library.Infrastructure.ApplicationContext;

namespace BookTour.Library.Infrastructure.Repository
{
    public class BookingRepository(BookTourContext context) : GenericRepository<Booking>(context), IBookingRepository
    {
        public Booking Add(Booking booking)
        {
            context.Bookings.Add(booking);
            context.SaveChanges();
            return booking;
        }

        
    }
}
