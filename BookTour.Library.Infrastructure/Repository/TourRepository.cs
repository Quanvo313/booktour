﻿using BookTour.Library.Application.IRepository;
using BookTour.Library.Domain.Entities;
using BookTour.Library.Infrastructure.ApplicationContext;
using Microsoft.EntityFrameworkCore;

namespace BookTour.Library.Infrastructure.Repository
{
    public class TourRepository(BookTourContext context) : GenericRepository<Tour>(context), ITourRepository
    {
        public IEnumerable<Tour> SearchByName(string name)
        {
            return context.Tours.Include(x => x.Images).Where(x => x.TourName.Contains(name)).ToList();
        }

        public IEnumerable<Tour> SearchByDestination(string destination)
        {
            return context.Tours.Include(x => x.Images).Where(x => x.Destination.Contains(destination)).ToList();
        }

        public IEnumerable<Tour> SearchByStart(string start)
        {
            return context.Tours.Include(x => x.Images).Where(x => x.Start.Contains(start)).ToList();
        }

        public IEnumerable<Tour> SearchByDate(DateTime startDate)
        {
            return context.Tours.Include(x => x.Images).Where(x => x.StartDate == startDate).ToList();
        }

        public IEnumerable<Tour> FilterByPrice(bool state)
        {
            IQueryable<Tour> query = context.Tours.Include(x => x.Images);
            if (state)
            {
                query = query.OrderBy(tour => tour.PricePerPerson);
            }
            else
            {
                query = query.OrderByDescending(tour => tour.PricePerPerson);
            }
            return query.ToList();
        }
    }
}
