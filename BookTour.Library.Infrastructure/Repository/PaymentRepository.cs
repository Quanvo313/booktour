﻿using BookTour.Library.Application.IRepository;
using BookTour.Library.Domain.Entities;
using BookTour.Library.Infrastructure.ApplicationContext;
using Microsoft.EntityFrameworkCore;

namespace BookTour.Library.Infrastructure.Repository
{
    public class PaymentRepository(BookTourContext context) : GenericRepository<Payment>(context), IPaymentRepository
    {
        public IEnumerable<Payment> GetPaymentByUserId(string userId)
        {
            var list = context.Payments.Include(x => x.Booking).Where(x => x.Booking.UserId.Equals(userId)).ToList();
            return list;
        }
    }
}
