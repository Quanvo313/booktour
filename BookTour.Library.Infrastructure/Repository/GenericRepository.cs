﻿using BookTour.Library.Application.IRepository;
using BookTour.Library.Infrastructure.ApplicationContext;
using Microsoft.EntityFrameworkCore;

namespace BookTour.Library.Infrastructure.Repository
{
    public class GenericRepository<TEntity>(BookTourContext context) : IGenericRepository<TEntity> where TEntity : class
    {
        public virtual void Add(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll(params string[] includeProperties)
        {
            IQueryable<TEntity> query = context.Set<TEntity>();

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.ToList();
        }

        public virtual TEntity GetById(int id, params string[] includeProperties)
        {   
            DbSet<TEntity> dbSet = context.Set<TEntity>();
            TEntity entity = dbSet.Find(id);
            if (entity != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    context.Entry(entity).Reference(includeProperty).Load();
                }
            }
            return entity;
        }

        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
