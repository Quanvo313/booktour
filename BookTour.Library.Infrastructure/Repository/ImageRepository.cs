﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Domain.Entities;
using BookTour.Library.Infrastructure.ApplicationContext;

namespace BookTour.Library.Infrastructure.Repository
{
    public class ImageRepository(BookTourContext context) : GenericRepository<Image>(context), IImageRepository
    {
        public IEnumerable<Image> GetImagesByTourId(int tourId)
        {
            var images = context.Images.Where(x => x.TourId == tourId).ToList();
            return images;
        }

    }
}
