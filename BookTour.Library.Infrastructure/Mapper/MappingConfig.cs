﻿using AutoMapper;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;
using BookTour.Share.Model.DTOs.Booking;
using BookTour.Share.Model.DTOs.Tour;
using BookTour.Share.Model.DTOs.Transportation;

namespace BookTour.Library.Infrastructure.Mapper
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<Image,ImageDTO>().ReverseMap();
                config.CreateMap<ItineraryDTO, Itinerary>().ReverseMap();

                config.CreateMap<Tour, TourDTO>()
                .ForMember(dest => dest.MainImage, opt => opt.MapFrom(src => src.Images.First(x => x.IsMain == true).ImageUrl))
                .ReverseMap();
                config.CreateMap<Tour, TourDetailsDTO>()
                .ForMember(dest => dest.TourTypeName, opt => opt.MapFrom(src => src.TourType.TourTypeName))
                .ForMember(dest => dest.TransportationName, opt => opt.MapFrom(src => src.Transportation.TransportationName))
                .ReverseMap();
                config.CreateMap<Tour, TourForCreationDTO>().ReverseMap();

                config.CreateMap<TourType, TourTypeDTO>().ReverseMap();

                config.CreateMap<Transportation,TransportationDTO>().ReverseMap();



                config.CreateMap<Booking, BookingDTO>().ReverseMap();
                config.CreateMap<Booking, BookingDetailsDTO>().ReverseMap();
                config.CreateMap<Booking, BookingForCreationDTO>().ReverseMap();

                config.CreateMap<Payment, PaymentDTO>().ReverseMap();
            });
            return mappingConfig;
        }
    }
}
