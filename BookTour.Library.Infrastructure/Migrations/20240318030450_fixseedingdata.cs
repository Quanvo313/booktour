﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookTour.Library.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class fixseedingdata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 18, 10, 4, 49, 827, DateTimeKind.Local).AddTicks(876));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 18, 10, 4, 49, 827, DateTimeKind.Local).AddTicks(895));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 18, 9, 8, 2, 12, DateTimeKind.Local).AddTicks(3458));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 18, 9, 8, 2, 12, DateTimeKind.Local).AddTicks(3507));
        }
    }
}
