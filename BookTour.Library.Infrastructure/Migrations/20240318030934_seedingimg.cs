﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookTour.Library.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class seedingimg : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 18, 10, 9, 34, 163, DateTimeKind.Local).AddTicks(1420));

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 13,
                column: "TourId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 14,
                column: "TourId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 15,
                column: "TourId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 18, 10, 9, 34, 163, DateTimeKind.Local).AddTicks(1454));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 18, 10, 4, 49, 827, DateTimeKind.Local).AddTicks(876));

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 13,
                column: "TourId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 14,
                column: "TourId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 15,
                column: "TourId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 18, 10, 4, 49, 827, DateTimeKind.Local).AddTicks(895));
        }
    }
}
