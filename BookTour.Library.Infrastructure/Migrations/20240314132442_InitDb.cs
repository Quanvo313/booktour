﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BookTour.Library.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class InitDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompanyDetails",
                columns: table => new
                {
                    CompanyDetailId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyDetailType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CompanyDetailDetail = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDetails", x => x.CompanyDetailId);
                });

            migrationBuilder.CreateTable(
                name: "TourTypes",
                columns: table => new
                {
                    TourTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TourTypeName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TourTypes", x => x.TourTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Transportations",
                columns: table => new
                {
                    TransportationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransportationName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transportations", x => x.TransportationId);
                });

            migrationBuilder.CreateTable(
                name: "Tours",
                columns: table => new
                {
                    TourId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TourName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Start = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TourTypeId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PricePerPerson = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TourDuration = table.Column<int>(type: "int", nullable: false),
                    TransportationId = table.Column<int>(type: "int", nullable: false),
                    Destination = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tours", x => x.TourId);
                    table.ForeignKey(
                        name: "FK_Tours_TourTypes_TourTypeId",
                        column: x => x.TourTypeId,
                        principalTable: "TourTypes",
                        principalColumn: "TourTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tours_Transportations_TransportationId",
                        column: x => x.TransportationId,
                        principalTable: "Transportations",
                        principalColumn: "TransportationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    BookingId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TourId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NumberOfPeople = table.Column<int>(type: "int", nullable: false),
                    BookingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TotalCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.BookingId);
                    table.ForeignKey(
                        name: "FK_Bookings_Tours_TourId",
                        column: x => x.TourId,
                        principalTable: "Tours",
                        principalColumn: "TourId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    ImageId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TourId = table.Column<int>(type: "int", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsMain = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.ImageId);
                    table.ForeignKey(
                        name: "FK_Images_Tours_TourId",
                        column: x => x.TourId,
                        principalTable: "Tours",
                        principalColumn: "TourId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    PaymentID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookingID = table.Column<int>(type: "int", nullable: false),
                    PaymentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PaymentMethod = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.PaymentID);
                    table.ForeignKey(
                        name: "FK_Payments_Bookings_BookingID",
                        column: x => x.BookingID,
                        principalTable: "Bookings",
                        principalColumn: "BookingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TourTypes",
                columns: new[] { "TourTypeId", "TourTypeName" },
                values: new object[,]
                {
                    { 1, "Adventure" },
                    { 2, "Relaxation" }
                });

            migrationBuilder.InsertData(
                table: "Transportations",
                columns: new[] { "TransportationId", "TransportationName" },
                values: new object[,]
                {
                    { 1, "Bus" },
                    { 2, "Plane" },
                    { 3, "Car" }
                });

            migrationBuilder.InsertData(
                table: "Tours",
                columns: new[] { "TourId", "Description", "Destination", "EndDate", "PricePerPerson", "Start", "StartDate", "TourDuration", "TourName", "TourTypeId", "TransportationId" },
                values: new object[,]
                {
                    { 1, "An exciting adventure tour.", "Hoa Binh", new DateTime(2023, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 500m, "Hồ Chí Minh", new DateTime(2023, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Amazing Adventure", 2, 1 },
                    { 2, "An exciting adventure tour.", "Hà Nội", new DateTime(2024, 10, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 500m, "Hồ Chí Minh", new DateTime(2024, 3, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Chuyến đi đáng nhớ", 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "Bookings",
                columns: new[] { "BookingId", "BookingDate", "NumberOfPeople", "TotalCost", "TourId", "UserId" },
                values: new object[] { 1, new DateTime(2024, 3, 14, 20, 24, 42, 415, DateTimeKind.Local).AddTicks(157), 2, 1000m, 1, "0c8c5fb5-b984-4344-b077-d660ac3f45f4" });

            migrationBuilder.InsertData(
                table: "Images",
                columns: new[] { "ImageId", "ImageText", "ImageUrl", "IsMain", "TourId" },
                values: new object[,]
                {
                    { 1, "A beautiful view", "https://vcdn1-dulich.vnecdn.net/2022/05/13/hohoabinh-1652415316-5109-1652415499.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=8KCD1P1Rg6b2V-MINSu3Bw", true, 1 },
                    { 2, "A beautiful view", "https://vukehoach.mard.gov.vn/atlas/prov/hoabinh/hoabinh.jpg", false, 1 },
                    { 3, "A beautiful view", "https://lh3.googleusercontent.com/BIiO6AXgkD42Pv26utjmStCmUULp2T98xTDX4W1sZyaaBUF0B21cy-i2O9hphR9LrTzSdMCqSayrXOT1MGes8lz2n1v_5Kok98MBMbKa2u10Vxb89sBXNRrOu7XJhhkhY6tOsy6pTk6T1Qki1aQ8EwGbwaOFLYf0rUSsad4J73lzn7Rqj-GEnWA_jqkbIg9acXJjmYlkPQ", false, 1 },
                    { 4, "A beautiful view", "https://bcp.cdnchinhphu.vn/334894974524682240/2023/2/7/hn-1675770617088584662218.jpg", true, 2 },
                    { 5, "A beautiful view", "https://statics.vinpearl.com/du-lich-ha-noi-4-ngay-3-dem-1_1681407076.jpg", false, 2 },
                    { 6, "A beautiful view", "https://thanhnien.mediacdn.vn/Uploaded/tiendung/2022_09_21/78b0bcc5-d37b-4301-8ee3-679d35df3c17-2002.jpeg", false, 2 }
                });

            migrationBuilder.InsertData(
                table: "Payments",
                columns: new[] { "PaymentID", "Amount", "BookingID", "PaymentDate", "PaymentMethod" },
                values: new object[] { 1, 1000m, 1, new DateTime(2024, 3, 14, 20, 24, 42, 415, DateTimeKind.Local).AddTicks(177), "Credit Card" });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_TourId",
                table: "Bookings",
                column: "TourId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_TourId",
                table: "Images",
                column: "TourId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_BookingID",
                table: "Payments",
                column: "BookingID");

            migrationBuilder.CreateIndex(
                name: "IX_Tours_TourTypeId",
                table: "Tours",
                column: "TourTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Tours_TransportationId",
                table: "Tours",
                column: "TransportationId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyDetails");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "Tours");

            migrationBuilder.DropTable(
                name: "TourTypes");

            migrationBuilder.DropTable(
                name: "Transportations");
        }
    }
}
