﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BookTour.Library.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class seedingdata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 18, 9, 8, 2, 12, DateTimeKind.Local).AddTicks(3458));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 18, 9, 8, 2, 12, DateTimeKind.Local).AddTicks(3507));

            migrationBuilder.InsertData(
                table: "Tours",
                columns: new[] { "TourId", "Description", "Destination", "EndDate", "PricePerPerson", "Start", "StartDate", "TourDuration", "TourName", "TourTypeId", "TransportationId" },
                values: new object[,]
                {
                    { 3, "Đi về miền tây.", "Cần Thơ", new DateTime(2024, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 500000m, "Hồ Chí Minh", new DateTime(2024, 3, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Miền tây mãi đỉnh", 1, 1 },
                    { 4, "Đi về miền tây.", "Hậu Giang", new DateTime(2024, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 500000m, "Hồ Chí Minh", new DateTime(2024, 3, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Miền tây mãi đỉnh phần 2", 1, 1 },
                    { 5, "Đi Đà Lạt vài ngày.", "Đà Lạt", new DateTime(2024, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 500000m, "Hồ Chí Minh", new DateTime(2024, 3, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Đi tìm cái lạnh", 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "Images",
                columns: new[] { "ImageId", "ImageText", "ImageUrl", "IsMain", "TourId" },
                values: new object[,]
                {
                    { 7, "A beautiful view", "https://upload.wikimedia.org/wikipedia/commons/c/c4/Can-tho-tuonglamphotos.jpg", true, 3 },
                    { 8, "A beautiful view", "https://ik.imagekit.io/tvlk/blog/2022/09/kinh-nghiem-du-lich-can-tho-1.jpg?tr=dpr-2,w-675", false, 3 },
                    { 9, "A beautiful view", "https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2022/12/2/1123288/Can-Tho-1.jpg", false, 3 },
                    { 10, "A beautiful view", "https://upload.wikimedia.org/wikipedia/commons/2/24/Th%C3%A1p_%C4%90%E1%BB%93ng_H%E1%BB%93_%E1%BB%9F_V%E1%BB%8B_Thanh.jpg", true, 4 },
                    { 11, "A beautiful view", "https://vcdn-vnexpress.vnecdn.net/2023/10/16/b09b04cdd9840eda5795-3735-1697439022.jpg", false, 4 },
                    { 12, "A beautiful view", "https://thanhnien.mediacdn.vn/Uploaded/dieutrang-qc/2022_07_04/vi-thanh-hau-giang-1-1238.jpg", false, 4 },
                    { 13, "A beautiful view", "https://ik.imagekit.io/tvlk/blog/2023/01/canh-dep-da-lat-1.jpg?tr=dpr-2,w-675", true, 4 },
                    { 14, "A beautiful view", "https://static.vinwonders.com/production/da-lat-duoc-menh-danh-la-gi-top-banner.jpg", false, 4 },
                    { 15, "A beautiful view", "https://statics.vinpearl.com/canh-dep-da-lat-1_1688379739.jpg", false, 4 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Images",
                keyColumn: "ImageId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tours",
                keyColumn: "TourId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tours",
                keyColumn: "TourId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tours",
                keyColumn: "TourId",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 14, 20, 41, 48, 932, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 14, 20, 41, 48, 932, DateTimeKind.Local).AddTicks(6325));
        }
    }
}
