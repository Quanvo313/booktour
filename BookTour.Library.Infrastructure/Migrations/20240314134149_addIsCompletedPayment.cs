﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookTour.Library.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class addIsCompletedPayment : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "Payments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 14, 20, 41, 48, 932, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                columns: new[] { "IsCompleted", "PaymentDate" },
                values: new object[] { true, new DateTime(2024, 3, 14, 20, 41, 48, 932, DateTimeKind.Local).AddTicks(6325) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "Payments");

            migrationBuilder.UpdateData(
                table: "Bookings",
                keyColumn: "BookingId",
                keyValue: 1,
                column: "BookingDate",
                value: new DateTime(2024, 3, 14, 20, 24, 42, 415, DateTimeKind.Local).AddTicks(157));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "PaymentID",
                keyValue: 1,
                column: "PaymentDate",
                value: new DateTime(2024, 3, 14, 20, 24, 42, 415, DateTimeKind.Local).AddTicks(177));
        }
    }
}
