﻿using BookTour.Library.Application.IService;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookTour.Service.API.Controllers
{
    [Route("[controller]")]
    [Authorize]
    [ApiController]
    public class PaymentController(IPaymentService paymentService) : ControllerBase
    {
        private ResponseDTO _response = new();

        [HttpGet]
        public ResponseDTO Get()
        {
            var Services = paymentService.GetPayments();
            _response.Result = Services;
            _response.Message = "Success";
            return _response;
        }

        [HttpGet("user/{id}")]
        public ResponseDTO GetByUserId(string id)
        {
            var Services = paymentService.GetPaymentsByUserId(id);
            _response.Result = Services;
            _response.Message = "Success";
            return _response;
        }

        // GET api/<ServiceController>/5
        [HttpGet("{id}")]
        public ResponseDTO Get(int id)
        {
            var Service = paymentService.GetPayment(id);
            _response.Result = Service;
            _response.Message = "Success";
            return _response;
        }



        // POST api/<ServiceController>
        [HttpPost]
        public ResponseDTO Post(PaymentDTO paymentDTO)
        {
            paymentService.Add(paymentDTO);
            _response.Message = "Success";
            return _response;
        }

        [HttpPut]
        public ResponseDTO Put(PaymentDTO paymentDTO)
        {
            paymentService.Update(paymentDTO);
            _response.Message = "Success";
            return _response;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public ResponseDTO Delete(int id)
        {
            paymentService.Delete(id);
            _response.Message = "Success";

            return _response;
        }
    }
}
