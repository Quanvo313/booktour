﻿using Microsoft.AspNetCore.Mvc;
using BookTour.Share.Model.DTOs;
using TourBooking.Library.Application.IService;
using BookTour.Share.Model.DTOs.Booking;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookTour.Service.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class BookingController(IBookingService bookingService) : ControllerBase
    {
        private ResponseDTO _response = new();

        [HttpGet]
        public ResponseDTO Get()
        {
            var Services = bookingService.GetBookings();
            _response.Result = Services;
            _response.Message = "Success";
            return _response;
        }

        
        [HttpGet("{id}")]
        public ResponseDTO Get(int id)
        {
            var Service = bookingService.GetBooking(id);
            _response.Result = Service;
            _response.Message = "Success";
            return _response;
        }

        [HttpPost]
        public ResponseDTO Post([FromBody] BookingDTO BookingForCreationDTO)
        {
            var result = bookingService.AddBooking(BookingForCreationDTO);
            _response.Result = result;
            _response.Message = "Success";
            return _response;
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public ResponseDTO Put(BookingForCreationDTO BookingForUpdateDTO)
        {
            bookingService.Update(BookingForUpdateDTO);
            _response.Message = "Success";

            return _response;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public ResponseDTO Delete(int id)
        {
            bookingService.DeleteBooking(id);
            _response.Message = "Success";
            return _response;
        }
    }
}
