﻿using Azure;
using BookTour.Library.Application.IService;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookTour.Service.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ImageController(IImageService imageService) : ControllerBase
    {
        private ResponseDTO _response = new();

        [AllowAnonymous]
        [HttpGet("tour/{tourid}")]
        public ResponseDTO Get(int tourid)
        {
            var Services = imageService.GetImagesByTourId(tourid);
            _response.Result = Services;
            _response.Message = "Success";
            return _response;
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseDTO GetAll()
        {
            var list = imageService.GetImages();
            _response.Result = list;
            _response.Message = "Success";
            return _response;
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public ResponseDTO GetById(int id)
        {
            var imageDetail = imageService.GetImage(id);
            _response.Result = imageDetail;
            _response.Message = "Success";
            return _response;
        }

        [HttpPost]
        public ResponseDTO Post(ImageDTO imageDTO)
        {
            imageService.AddImage(imageDTO);
            _response.Message = "Success";
            return _response;
        }

        [HttpPut]
        public ResponseDTO Put(ImageDTO imageDTO)
        {
            imageService.UpdateImage(imageDTO);
            _response.Message = "Success";
            return _response;
        }

        [HttpDelete]
        public ResponseDTO Delete(int id)
        {
            imageService.DeleteIImage(id);
            _response.Message = "Success";
            return _response;
        }
    }
}
