﻿using Microsoft.AspNetCore.Mvc;
using BookTour.Library.Application.IService;
using BookTour.Share.Model.DTOs;
using BookTour.Share.Model.DTOs.Tour;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookTour.Service.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TourController(ITourService tourService) : ControllerBase
    {
        private ResponseDTO _response = new();

        [AllowAnonymous]
        [HttpGet]
        public ResponseDTO Get()
        {
            var tours = tourService.GetTours();
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        [HttpGet("name/{name}")]
        public ResponseDTO GetByName(string name)
        {
            var tours = tourService.SearchByName(name);
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        [HttpGet("start/{start}")]
        public ResponseDTO GetByStart(string start)
        {
            var tours = tourService.SearchByStart(start);
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        [HttpGet("date/{startDate}")]
        public ResponseDTO GetByStart(DateTime startDate)
        {
            var tours = tourService.SearchByDate(startDate);
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        [HttpGet("filter/price/{state}")]
        public ResponseDTO FilterPrice(bool state)
        {
            var tours = tourService.FilterByPrice(state);
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }


        [AllowAnonymous]
        [HttpGet("destination/{destination}")]
        public ResponseDTO GetByDestination(string destination)
        {
            var tours = tourService.SearchByDestination(destination);
            _response.Result = tours;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        // GET api/<TourController>/5
        [HttpGet("{id}")]
        public ResponseDTO Get(int id)
        {
            var tour = tourService.GetTour(id);
            _response.Result = tour;
            _response.Message = "Success";

            return _response;
        }

        // POST api/<TourController>
        [HttpPost]
        public ResponseDTO Post([FromBody] TourForCreationDTO tourForCreationDTO)
        {
            tourService.AddTour(tourForCreationDTO);
            _response.Message = "Success";
            return _response;
        }

        [HttpPut]
        public ResponseDTO Put(TourForCreationDTO tourForUpdateDTO)
        {
            tourService.Update(tourForUpdateDTO);
            _response.Message = "Success";
            return _response;
        }

        [HttpDelete]
        public ResponseDTO Delete(int id)
        {
            tourService.DeleteTour(id);
            _response.Message = "Success";
            return _response;
        }
    }
}
