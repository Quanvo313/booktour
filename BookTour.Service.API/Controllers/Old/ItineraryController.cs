﻿//using BookTour.Library.Application.IService;
//using BookTour.Share.Model.DTOs;
//using Microsoft.AspNetCore.Mvc;

//namespace BookTour.Service.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class ItineraryController(IItineraryService itineraryService) : ControllerBase
//    {
//        private ResponseDTO _response = new();

//        [HttpGet]
//        public ResponseDTO GetAll()
//        {
//            var list = itineraryService.GetItineraries();
//            _response.Result = list;
//            _response.Message = "Success";
//            return _response;
//        }

//        [HttpGet("{id}")]
//        public ResponseDTO GetById(int id)
//        {
//            var itinerary = itineraryService.GetItinerary(id);
//            _response.Result = itinerary;
//            _response.Message = "Success";
//            return _response;
//        }

//        [HttpPost]
//        public ResponseDTO Post(ItineraryDTO itineraryDTO)
//        {
//            itineraryService.AddItinerary(itineraryDTO);
//            _response.Message = "Success";
//            return _response;
//        }

//        [HttpPut]
//        public ResponseDTO Put(ItineraryDTO itineraryDTO)
//        {
//            itineraryService.Update(itineraryDTO);
//            _response.Message = "Success";
//            return _response;
//        }

//        [HttpDelete]
//        public ResponseDTO Delete(int id)
//        {
//            itineraryService.DeleteItinerary(id);
//            _response.Message = "Success";
//            return _response;
//        }
//    }
//}
