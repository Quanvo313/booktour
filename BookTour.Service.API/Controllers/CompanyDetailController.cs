﻿using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookTour.Service.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class CompanyDetailController(ICompanyDetailService companyDetailService) : ControllerBase
    {
        private ResponseDTO _response = new();
        
        [HttpGet]
        [AllowAnonymous]
        public ResponseDTO GetAll()
        {
            var list = companyDetailService.GetAll();
            _response.Result = list;
            _response.Message = "Success";
            return _response;
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public ResponseDTO GetById(int id)
        {
            var companyDetail = companyDetailService.GetCompanyDetail(id);
            _response.Result = companyDetail;
            _response.Message = "Success";
            return _response;
        }

        [HttpPost]
        public ResponseDTO Post(CompanyDetailDTO companyDetail)
        {
            companyDetailService.AddCompanyDetail(companyDetail);
            _response.Message = "Success";
            return _response;
        }

        [HttpPut]
        public ResponseDTO Put(CompanyDetailDTO companyDetail)
        {
            companyDetailService.UpdateCompanyDetail(companyDetail);
            _response.Message = "Success";
            return _response;
        }

        [HttpDelete]
        public ResponseDTO Delete(int id)
        {
            companyDetailService.DeleteCompanyDetail(id);
            _response.Message = "Success";
            return _response;
        }
    }
}
