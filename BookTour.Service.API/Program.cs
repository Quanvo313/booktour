using AutoMapper;
using BookTour.Library.Application;
using BookTour.Library.Infrastructure;
using BookTour.Library.Infrastructure.ApplicationContext;
using BookTour.Library.Infrastructure.Mapper;
using BookTour.Service.API.Extensions;
using BookTour.Service.API.Middleware;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//repository
builder.Services.AddRepository();
//service
builder.Services.AddServices();



builder.AddAppAuthentication();
builder.Services.AddAuthorization();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseMiddleware<ExceptionMiddleware>();
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
