﻿
using BookTour.Service.Authentication.Models;

namespace BookTour.Service.Authentication.Service.IService
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(ApplicationUser applicationUser, IEnumerable<string> roles);
    }
}
