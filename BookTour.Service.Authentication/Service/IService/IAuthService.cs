﻿
using BookTour.Service.Authentication.Models;
using BookTour.Share.Model.DTOs;

namespace BookTour.Service.Authentication.Service.IService
{
    public interface IAuthService
    {
        Task<ApplicationUser> Register(RegistrationRequestDTO registrationRequestDTO);
        Task<LoginResponseDTO> Login(LoginRequestDTO loginRequestDTO);
        Task<bool> AssignRole(string email, string roleName);
        Task<bool> ResetPassword(string email, string token, string newPassword);
        Task<string> GeneratePasswordResetToken(string email);
        Task<bool> ChangePassword(string email, string currentPassword, string newPassword);
        Task<string> UpdateUserProfile(UpdateProfileRequestDTO updateProfileRequestDTO);
        Task<UpdateProfileRequestDTO> GetUserProfile(string userId);
    }
}
