﻿namespace BookTour.Service.Authentication.Service.IService
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string fromAddress, string toAddress, string subject, string messsage);
    }
}
