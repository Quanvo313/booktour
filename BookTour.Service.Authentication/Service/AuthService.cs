﻿using BookTour.Service.Authentication.Data;
using BookTour.Service.Authentication.Models;
using BookTour.Service.Authentication.Service.IService;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Identity;

namespace BookTour.Service.Authentication.Service
{
    public class AuthService(
        AppDbContext db,
        UserManager<ApplicationUser> userManager,
        RoleManager<IdentityRole> roleManager,
        IJwtTokenGenerator jwtTokenGenerator) : IAuthService
    {
        public async Task<bool> AssignRole(string email, string roleName)
        {
            var user = db.ApplicationUsers.FirstOrDefault(u => u.Email.Equals(email));
            if (user != null)
            {
                if (!roleManager.RoleExistsAsync(roleName).GetAwaiter().GetResult())
                {
                    roleManager.CreateAsync(new IdentityRole(roleName)).GetAwaiter().GetResult();
                }
                await userManager.AddToRoleAsync(user, roleName);
                return true;
            }
            return false;
        }

        public async Task<LoginResponseDTO> Login(LoginRequestDTO loginRequestDTO)
        {

            var user = db.ApplicationUsers.FirstOrDefault(u => u.UserName.Equals(loginRequestDTO.UserName));
            if (user == null)
            {
                return new LoginResponseDTO() { User = null, Token = "" };
            }
            var isConfirmEmail = await userManager.IsEmailConfirmedAsync(user);
            if (!isConfirmEmail)
            {
                return new LoginResponseDTO() { User = null, Message = "Email is not confirmed." };
            }
            bool isValid = await userManager.CheckPasswordAsync(user, loginRequestDTO.Password);

            if (!isValid)
            {
                await userManager.AccessFailedAsync(user);
                if (await userManager.IsLockedOutAsync(user))
                {
                    return new LoginResponseDTO() { User = null, Message = "Account is locked. Please try again later." };
                }
                return new LoginResponseDTO() { User = null, Message = "Invalid credentials. Please try again." };
            }
            if (await userManager.IsLockedOutAsync(user))
            {
                return new LoginResponseDTO() { User = null, Token = "" };
            }
            var roles = await userManager.GetRolesAsync(user);
            var token = jwtTokenGenerator.GenerateToken(user, roles);
            UserDTO userDto = new()
            {
                Email = user.Email,
                ID = user.Id,
                Name = user.FullName,
                PhoneNumber = user.PhoneNumber,
            };
            LoginResponseDTO responseDTO = new()
            {
                User = userDto,
                Token = token
            };
            return responseDTO;
        }

        public async Task<ApplicationUser> Register(RegistrationRequestDTO registrationRequestDTO)
        {
            ApplicationUser user = new()
            {
                UserName = registrationRequestDTO.Email,
                Email = registrationRequestDTO.Email,
                NormalizedEmail = registrationRequestDTO.Email.ToUpper(),
                FullName = registrationRequestDTO.FullName,
                PhoneNumber = registrationRequestDTO.PhoneNumber,
            };
            var isRoleExists = await roleManager.RoleExistsAsync("User");
            if (!isRoleExists)
            {
                await roleManager.CreateAsync(new IdentityRole("User"));
            }
            var result = await userManager.CreateAsync(user, registrationRequestDTO.Password);
            await AssignRole(user.Email, "User");
            if (result.Succeeded)
            {
                var userToReturn = db.ApplicationUsers.First(u => u.UserName == registrationRequestDTO.Email);
                return userToReturn;

            }
            return null;

        }

        public async Task<string> GeneratePasswordResetToken(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user == null) return null;
            if (!await userManager.IsEmailConfirmedAsync(user)) return null;
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            return token;
        }

        public async Task<bool> ResetPassword(string email, string token, string newPassword)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user == null) return false; // User not found
            var result = await userManager.ResetPasswordAsync(user, token, newPassword);
            return result.Succeeded;
        }

        public async Task<bool> ChangePassword(string email, string currentPassword, string newPassword)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user == null) return false; // User not found
            var result = await userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            return result.Succeeded;
        }

        public async Task<string> UpdateUserProfile(UpdateProfileRequestDTO updateProfileRequestDTO)
        {
            var user = await userManager.FindByIdAsync(updateProfileRequestDTO.UserId);

            if (user == null)
            {
                return "";
            }

            user.FullName = updateProfileRequestDTO.Name;
            user.PhoneNumber = updateProfileRequestDTO.PhoneNumber;
            user.Email = updateProfileRequestDTO.Email;

            var result = await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return "Profile updated successfully";
            }
            else
            {
                return result.Errors.FirstOrDefault()?.Description ?? "Profile update failed";
            }
        }

        public async Task<UpdateProfileRequestDTO> GetUserProfile(string userId)
        {

            var user = await userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return null;
            }

            var userDto = new UpdateProfileRequestDTO
            {
                UserId = user.Id,
                Email = user.Email,
                Name = user.FullName,
                PhoneNumber = user.PhoneNumber,
            };

            return userDto;
        }
    }
}
