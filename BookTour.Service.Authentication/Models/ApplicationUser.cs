﻿using Microsoft.AspNetCore.Identity;

namespace BookTour.Service.Authentication.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
    }
}
