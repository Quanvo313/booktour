﻿using BookTour.Service.Authentication.Models;
using BookTour.Service.Authentication.Service.IService;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Web;

namespace BookTour.Service.Authentication.Controllers
{
    [ApiController]
    public class AuthAPIController(IAuthService authService, IEmailSender emailSender, UserManager<ApplicationUser> userManager, IConfiguration configuration) : ControllerBase
    {
        protected ResponseDTO _response = new();

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequestDTO model)
        {
            var user = await authService.Register(model);
            if (user != null)
            {
                var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                var uriBuider = new UriBuilder(configuration["ReturnPaths:ConfirmEmail"]);
                var query = HttpUtility.ParseQueryString(uriBuider.Query);
                query["token"] = token;
                query["userid"] = user.Id;
                uriBuider.Query = query.ToString();
                var senderEmail = configuration["ReturnPaths:SenderEmail"];
                var urlString = uriBuider.ToString();

                var url = $"https://localhost:7138/verify-email?token={token}&id={user.Id}";
                var emailContent = $@"
<html>
<head>
    <style>
        .button {{display: inline-block;
            padding: 10px 20px;
            font-size: 14px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 15px;
            box-shadow: 0 9px #999;
        
            }}

        .button:hover {{background-color: #3e8e41}}

        .button:active {{background-color: #3e8e41;
            box-shadow: 0 5px #666;
            transform: translateY(4px);
        }}
    </style>
</head>
<body>
    <p>Please click on the button below to verify email:</p>
    <a href={urlString} class='button'>Verify Email</a>
</body>
</html>";
                await emailSender.SendEmailAsync(senderEmail, user.Email, "Chào mừng đến với nụ cười ép bê tê", emailContent);
                _response.Message = "Register successfuly";
                _response.IsSucess = true;
                return Ok(_response);
            }
            _response.Message = "Fail";
            _response.IsSucess = false;
            return Ok(_response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequestDTO model)
        {
            var loginResponse = await authService.Login(model);

            if (loginResponse.User == null)
            {
                _response.IsSucess = false;
                _response.Message = loginResponse.Message == null ? "Đăng nhập thất bại" : loginResponse.Message;
                return BadRequest(_response);
            }
            _response.Result = loginResponse;
            return Ok(_response);
        }

        [HttpGet]
        [Route("verify-email")]
        public async Task<IActionResult> VerifyEmail(string token, string userid)
        {
            var user = await userManager.FindByIdAsync(userid);
            if (user == null)
            {
                return BadRequest("User does not exists");
            }
            var result = await userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                return Ok("Email verify successfully");
            }
            return BadRequest("Can't verify your email");
        }

        [HttpPost("forget-password")]
        public async Task<IActionResult> ForgetPassword([FromBody] ForgetPasswordRequestDTO model)
        {
            var token = await authService.GeneratePasswordResetToken(model.Email);
            if (string.IsNullOrEmpty(token))
            {
                _response.Message = "Email cannot be found or email has not been confirmed";
                _response.IsSucess = false;
                return BadRequest(_response);
            }

            var passwordResetLink = $"https://localhost:7138/authen/reset-password?token={token}&email={model.Email}";

            var senderEmail = configuration["ReturnPaths:SenderEmail"];
            var emailContent = $@"
<html>
<head>
    <style>
        .button {{display: inline-block;
            padding: 10px 20px;
            font-size: 14px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 15px;
            box-shadow: 0 9px #999;
        
            }}

        .button:hover {{background-color: #3e8e41}}

        .button:active {{background-color: #3e8e41;
            box-shadow: 0 5px #666;
            transform: translateY(4px);
        }}
    </style>
</head>
<body>
    <p>Please click on the button below to reset your password:</p>
    <a href={passwordResetLink} class='button'>Reset Password</a>
</body>
</html>";

            await emailSender.SendEmailAsync(senderEmail, model.Email, "Forget Password", emailContent);

            _response.Message = "Password reset link has been sent to your email";
            _response.IsSucess = true;
            return Ok(_response);
        }



        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDTO model)
        {
            var result = await authService.ResetPassword(model.Email, model.Token, model.NewPassword);
            if (!result)
            {
                _response.Message = "Password reset failed";
                _response.IsSucess = false;
                return BadRequest(_response);
            }

            _response.Message = "Password has been successfully reset";
            _response.IsSucess = true;
            return Ok(_response);
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO model)
        {
            var result = await authService.ChangePassword(model.Email, model.CurrentPassword, model.NewPassword);
            if (!result)
            {
                _response.Message = "Password change failed";
                _response.IsSucess = false;
                return BadRequest(_response);
            }

            _response.Message = "Password has been successfully changed";
            _response.IsSucess = true;
            return Ok(_response);
        }

        [HttpPut("update-profile")]
        public async Task<IActionResult> UpdateProfile([FromBody] UpdateProfileRequestDTO model)
        {
            var message = await authService.UpdateUserProfile(model);
            if (string.IsNullOrEmpty(message))
            {
                message = "Error";
                return BadRequest(message);
            }
            return Ok(message);
        }

        [HttpGet("get-profile/{id}")]
        public async Task<IActionResult> GetProfile(string id)
        {
            var user = await authService.GetUserProfile(id);
            if (user == null)
            {
                return NotFound(user);
            }
            return Ok(user);
        }

    }
}
