﻿namespace BookTour.Share.Model.DTOs.Tour
{
    public class TourDTO
    {
        public int TourId { get; set; }
        public string Start { get; set; }
        public string TourName { get; set; }
        public int TourTypeId { get; set; }
        public string Description { get; set; }
        public decimal PricePerPerson { get; set; }
        public int TourDuration { get; set; }
        public int TransportationId { get; set; }
        public string Destination { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string? MainImage { get; set; }
    }
}
