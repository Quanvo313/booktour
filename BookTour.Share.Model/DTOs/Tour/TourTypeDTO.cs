﻿namespace BookTour.Share.Model.DTOs.Tour
{
    public class TourTypeDTO
    {
       public int TourTypeId { get; set; }
        public string TourTypeName { get; set; }
    }
}
