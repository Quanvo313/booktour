﻿using BookTour.Share.Model.DTOs.Transportation;

namespace BookTour.Share.Model.DTOs.Tour
{
    public class TourDetailsDTO
    {
        public int TourId { get; set; }
        public string TourName { get; set; }
        public string Start { get; set; }
        //tour type
        public string TourTypeName { get; set; }
        public int TourTypeId { get; set; }
        public string Description { get; set; }
        public decimal PricePerPerson { get; set; }
        public int TourDuration { get; set; }
        public string Destination { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //transportation
        public string TransportationName { get; set; }
        public int TransportationId { get; set; }
    }
}
