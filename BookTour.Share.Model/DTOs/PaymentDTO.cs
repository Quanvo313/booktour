﻿namespace BookTour.Share.Model.DTOs
{
    public class PaymentDTO
    {
        public int PaymentID { get; set; }
        public int BookingID { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public bool IsCompleted { get; set; } = false;
    }
}
