﻿namespace BookTour.Share.Model.DTOs
{
    public class ItineraryDTO
    {
        public int ItineraryId { get; set; }
        public int NumberOfDays { get; set; }
        public int TourId { get; set; }
    }
}
