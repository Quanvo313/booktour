﻿namespace BookTour.Share.Model.DTOs
{
    public class ImageDTO
    {
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
        public string ImageText { get; set; }
        public bool IsMain { get; set; } = false;
        public int TourId { get; set; }
    }
}
