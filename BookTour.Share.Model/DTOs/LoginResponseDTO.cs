﻿namespace BookTour.Share.Model.DTOs
{
    public class LoginResponseDTO
    {
        public UserDTO User { get; set; }
        public string Token { get; set; }
        public string? Message { get; set; }
    }
}
