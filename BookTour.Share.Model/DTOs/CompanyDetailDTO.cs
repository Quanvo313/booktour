﻿namespace BookTour.Share.Model.DTOs
{
    public class CompanyDetailDTO
    {
        public int CompanyDetailId { get; set; }
        public string CompanyDetailType { get; set; }
        public string CompanyDetailDetail { get; set; }
    }
}
