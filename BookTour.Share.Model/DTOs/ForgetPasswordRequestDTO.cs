﻿using System.ComponentModel.DataAnnotations;

namespace BookTour.Share.Model.DTOs
{
    public class ForgetPasswordRequestDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
