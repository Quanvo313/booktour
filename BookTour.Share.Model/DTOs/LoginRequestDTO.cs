﻿using System.ComponentModel.DataAnnotations;

namespace BookTour.Share.Model.DTOs
{
    public class LoginRequestDTO
    {
        [Required]
        [EmailAddress]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
