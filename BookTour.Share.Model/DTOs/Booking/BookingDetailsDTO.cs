﻿using BookTour.Share.Model.DTOs.Tour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookTour.Share.Model.DTOs.Booking
{
    public class BookingDetailsDTO : BookingDTO
    {
        public decimal TotalCost { get; set; } = decimal.Zero;
        public TourDTO Tour { get; set; }
    }
}
