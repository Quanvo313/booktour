﻿namespace BookTour.Share.Model.DTOs.Booking
{
    public class BookingForCreationDTO : BookingDTO
    {
        public decimal TotalCost { get; set; } = decimal.Zero;
    }
}
