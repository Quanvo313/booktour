﻿using System.ComponentModel.DataAnnotations;

namespace BookTour.Share.Model.DTOs
{
    public class UpdateProfileRequestDTO
    {
        public string? UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
