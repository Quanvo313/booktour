﻿namespace BookTour.Share.Model.DTOs.Transportation
{
    public class TransportationDTO
    {
        public int TransportationId { get; set; }
        public string TransportationName { get; set; }
    }
}
