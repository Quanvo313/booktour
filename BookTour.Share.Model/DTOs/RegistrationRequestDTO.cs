﻿using System.ComponentModel.DataAnnotations;

namespace BookTour.Share.Model.DTOs
{
    public class RegistrationRequestDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        [Required]
        public string FullName { get; set; }

        public string? Role { get; set; } = "User";
        [Required]  
        public string Password { get; set; }
    }
}
