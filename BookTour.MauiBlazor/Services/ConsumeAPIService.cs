﻿using BookTour.MauiBlazor.IServices;
using BookTour.Share.Model.DTOs;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace BookTour.MauiBlazor.Services
{
    public class ConsumeAPIService : IConsumeAPIService
    {
        private readonly HttpClient _httpClient;
        private readonly string _baseUrl;
        private string _jwtToken;
        public ConsumeAPIService()
        {
            _httpClient = new HttpClient();
            _baseUrl = "https://localhost:7000/";
            _httpClient.BaseAddress = new Uri(_baseUrl);
        }

        public void SetBearerToken(string token)
        {
            _jwtToken = token;
        }

        public async Task<string> LoginAsync(LoginRequestDTO loginRequestDTO)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync($"authen/login", loginRequestDTO);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    return responseData;
                }
                else
                {
                    throw new Exception("Failed to log in");
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<string> RegisterAsync(RegistrationRequestDTO registrationRequestDTO)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync($"authen/register", registrationRequestDTO);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    return responseData;
                }
                else
                {
                    throw new Exception("Failed to log in");
                }
            }
            catch (Exception)
            {

                throw;
            }

        }


        public async Task<string> GetAsync(string endpoint)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
            var response = await _httpClient.GetAsync($"{endpoint}");
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                throw new Exception($"Failed to get data from {endpoint}");
            }
        }

        public async Task<string> PostAsync(string endpoint, object jsonContent)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
            try
            {
                var response = await _httpClient.PostAsJsonAsync($"{endpoint}", jsonContent);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {

                throw new Exception($"Failed to post data to {endpoint}");
            }

        }

        public async Task<string> PutAsync(string endpoint, object jsonContent)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);

            var response = await _httpClient.PutAsJsonAsync($"{endpoint}", jsonContent);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                throw new Exception($"Failed to put data to {endpoint}");
            }
        }

        public async Task<string> DeleteAsync(string endpoint)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);

            var response = await _httpClient.DeleteAsync($"{_baseUrl}/{endpoint}");
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                throw new Exception($"Failed to delete data from {endpoint}");
            }
        }

        public async Task<string> GetALlTourAsync()
        {

            var response = await _httpClient.GetAsync("api/tour");
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                throw new Exception($"Failed to get data from api");
            }
        }


    }
}
