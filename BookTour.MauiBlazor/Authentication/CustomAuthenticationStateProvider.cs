﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.Security.Claims;

namespace BookTour.MauiBlazor.Authentication
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly IJSRuntime _jsRuntime;
        private ClaimsPrincipal _currentUser = new ClaimsPrincipal(new ClaimsIdentity());

        public CustomAuthenticationStateProvider(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", "userToken");
            if (!string.IsNullOrEmpty(token))
            {
                var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, "UserFromToken") }, "jwtAuthType");
                _currentUser = new ClaimsPrincipal(identity);
            }
            else
            {
                var accountId = await _jsRuntime.InvokeAsync<string>("localStorage.getItem", new object[] { "accountId" });
                if (!string.IsNullOrEmpty(accountId))
                {
                    var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.NameIdentifier, accountId) }, "customAuthType");
                    _currentUser = new ClaimsPrincipal(identity);
                }
            }

            return new AuthenticationState(_currentUser);
        }

        public void MarkUserAsAuthenticated(string username, string id, string jwt)
        {
            var authenticatedUser = new ClaimsPrincipal(new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.NameIdentifier, id),
                new Claim(ClaimTypes.Authentication, jwt)
            }, "customAuthType")) ;
            _currentUser = authenticatedUser;
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(_currentUser)));
        }

        public async Task MarkUserAsLoggedOut()
        {
            _currentUser = new ClaimsPrincipal(new ClaimsIdentity());
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(_currentUser)));

            await _jsRuntime.InvokeVoidAsync("localStorage.removeItem", "userToken");
            await _jsRuntime.InvokeVoidAsync("localStorage.removeItem", "accountId");
        }
    }

}
