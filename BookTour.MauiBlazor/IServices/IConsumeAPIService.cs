﻿using BookTour.Share.Model.DTOs;

namespace BookTour.MauiBlazor.IServices
{
    public interface IConsumeAPIService
    {
        Task<string> LoginAsync(LoginRequestDTO loginRequestDTO);
        Task<string> GetAsync(string endpoint);
        Task<string> GetALlTourAsync();
        Task<string> PostAsync(string endpoint, object jsonContent);
        Task<string> PutAsync(string endpoint, object jsonContent);
        Task<string> DeleteAsync(string endpoint);
        public void SetBearerToken(string token);
    }
}
