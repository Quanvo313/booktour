﻿namespace BookTour.MauiBlazor.IServices
{
    public interface ITokenProvider
    {
        void SetToken(string token);    
        string? GetToken();
        void ClearToken();
    }
}
