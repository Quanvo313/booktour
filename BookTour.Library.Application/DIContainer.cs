﻿using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Application.Serivce;
using Microsoft.Extensions.DependencyInjection;
using TourBooking.Library.Application.IService;

namespace BookTour.Library.Application
{
    public static class DIContainer
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            //service
            services.AddScoped<ICompanyDetailService, CompanyDetailService>();
            services.AddScoped<IItineraryService, ItineraryService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<ITourService, TourService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<IPaymentService, PaymentService>();
            return services;
        }

    }
}
