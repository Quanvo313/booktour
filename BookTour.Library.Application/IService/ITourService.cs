﻿using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs.Tour;

namespace BookTour.Library.Application.IService
{
    public interface ITourService
    {
        public IEnumerable<TourDTO> GetTours();
        public TourDetailsDTO GetTour(int id);
        public void AddTour(TourForCreationDTO dto);
        public void Update(TourForCreationDTO dto);
        public void DeleteTour(int id);
        IEnumerable<TourDTO> SearchByName(string name);
        IEnumerable<TourDTO> SearchByDestination(string destination);
        IEnumerable<TourDTO> SearchByStart(string start);
        IEnumerable<TourDTO> SearchByDate(DateTime startDate);
        IEnumerable<TourDTO> FilterByPrice(bool state);
    }
}
