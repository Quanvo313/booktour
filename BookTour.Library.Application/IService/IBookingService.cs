﻿using BookTour.Share.Model.DTOs.Booking;

namespace TourBooking.Library.Application.IService
{
    public interface IBookingService
    {
        public IEnumerable<BookingDTO> GetBookings();
        public BookingDetailsDTO GetBooking(int id);
        public BookingDTO AddBooking(BookingDTO dto);
        public void Update(BookingForCreationDTO dto);
        public void DeleteBooking(int id);
    }
}
