﻿using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.IService
{
    public interface IImageService
    {
        public IEnumerable<ImageDTO> GetImages();
        public ImageDTO GetImage(int id);
        public void AddImage(ImageDTO dto);
        public void UpdateImage(ImageDTO dto);
        public void DeleteIImage(int id);
        public IEnumerable<ImageDTO> GetImagesByTourId(int tourId);
    }
}
