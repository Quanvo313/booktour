﻿using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.IService
{
    public interface ICompanyDetailService
    {
        IEnumerable<CompanyDetailDTO> GetAll();
        public CompanyDetailDTO GetCompanyDetail(int id);
        public void AddCompanyDetail(CompanyDetailDTO dto);
        public void UpdateCompanyDetail(CompanyDetailDTO dto);
        public void DeleteCompanyDetail(int id);
    }
}
