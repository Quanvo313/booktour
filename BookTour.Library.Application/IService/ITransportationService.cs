﻿using BookTour.Share.Model.DTOs.Transportation;

namespace BookTour.Library.Application.IService
{
    public interface ITransportationService
    {
        public IEnumerable<TransportationDTO> GetTransportations();
        public TransportationDTO GetTransportation(int id);
        public void Add(TransportationDTO dto);
        public void Update(TransportationDTO dto);
        public void Delete(int id);
    }
}
