﻿using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.IService
{
    public interface IPaymentService
    {
        public IEnumerable<PaymentDTO> GetPayments();
        public IEnumerable<PaymentDTO> GetPaymentsByUserId(string userId);
        public PaymentDTO GetPayment(int id);
        public void Add(PaymentDTO dto);
        public void Update(PaymentDTO dto);
        public void Delete(int id);

    }
}
