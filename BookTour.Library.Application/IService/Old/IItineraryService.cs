﻿using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.IService
{
    public interface IItineraryService
    {
        public IEnumerable<ItineraryDTO> GetItineraries();
        public ItineraryDTO GetItinerary(int id);
        public void AddItinerary(ItineraryDTO dto);
        public void Update(ItineraryDTO dto);
        public void DeleteItinerary(int id);
    }
}
