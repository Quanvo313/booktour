﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.Serivce
{
    public class PaymentService(IPaymentRepository paymentRepository, IMapper mapper) : IPaymentService
    {
        public void Add(PaymentDTO dto)
        {
            var model = mapper.Map<Payment>(dto);
            paymentRepository.Add(model);
        }

        public void Delete(int id)
        {
            var model = paymentRepository.GetById(id);
            paymentRepository.Delete(model);
        }

        public PaymentDTO GetPayment(int id)
        {
            var model = paymentRepository.GetById(id);
            var dto = mapper.Map<PaymentDTO>(model);
            return dto;
        }

        public IEnumerable<PaymentDTO> GetPayments()
        {
            var models = paymentRepository.GetAll();
            var dtos = mapper.Map<IEnumerable<PaymentDTO>>(models);
            return dtos;
        }

        public IEnumerable<PaymentDTO> GetPaymentsByUserId(string userId)
        {
            var models = paymentRepository.GetPaymentByUserId(userId);
            var dtos = mapper.Map<IEnumerable<PaymentDTO>>(models);
            return dtos;
        }

        public void Update(PaymentDTO dto)
        {
            var model = mapper.Map<Payment>(dto);
            paymentRepository.Update(model);
        }
    }
}
