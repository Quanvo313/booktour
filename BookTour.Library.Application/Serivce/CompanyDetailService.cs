﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.Serivce
{
    public class CompanyDetailService(IGenericRepository<CompanyDetail> genericRepository, IMapper mapper) : ICompanyDetailService
    {
        public void AddCompanyDetail(CompanyDetailDTO dto)
        {
            var companyDetail = mapper.Map<CompanyDetail>(dto);
            genericRepository.Add(companyDetail);
        }

        public void DeleteCompanyDetail(int id)
        {
            var companydetail = genericRepository.GetById(id);
            genericRepository.Delete(companydetail);
        }

        public IEnumerable<CompanyDetailDTO> GetAll()
        {
            var list = genericRepository.GetAll(); 
            var listDto = mapper.Map<IEnumerable<CompanyDetailDTO>>(list);
            return listDto;
        }

        public CompanyDetailDTO GetCompanyDetail(int id)
        {
            var companyDetail = genericRepository.GetById(id);
            var dto = mapper.Map<CompanyDetailDTO>(companyDetail);
            return dto;
        }

        public void UpdateCompanyDetail(CompanyDetailDTO dto)
        {
            var companyDetail = mapper.Map<CompanyDetail>(dto);
            genericRepository.Update(companyDetail);
        }
    }
}
