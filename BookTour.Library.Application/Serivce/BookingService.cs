﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs.Booking;
using TourBooking.Library.Application.IService;

namespace BookTour.Library.Application.Serivce
{
    public class BookingService(IBookingRepository bookingRepository, IMapper mapper) : IBookingService
    {
        public BookingDTO AddBooking(BookingDTO dto)
        {
            var Booking = mapper.Map<Booking>(dto);
            var result = bookingRepository.Add(Booking);
            var BookingToReturn = mapper.Map<BookingDTO>(result);
            return BookingToReturn;
        }

        public void DeleteBooking(int id)
        {
            var Booking = bookingRepository.GetById(id);
            bookingRepository.Delete(Booking);
        }

        public IEnumerable<BookingDTO> GetBookings()
        {
            var Bookings = bookingRepository.GetAll();
            var BookingsDTO = mapper.Map<IEnumerable<BookingDTO>>(Bookings);
            return BookingsDTO;
        }

        public BookingDetailsDTO GetBooking(int id)
        {
            var Booking = bookingRepository.GetById(id);
            var BookingDTO = mapper.Map<BookingDetailsDTO>(Booking);
            return BookingDTO;
        }

        public void Update(BookingForCreationDTO dto)
        {
            var Booking = mapper.Map<Booking>(dto);
            bookingRepository.Update(Booking);
        }
    }
}
