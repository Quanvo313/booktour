﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.Serivce
{
    public class ItineraryService(IGenericRepository<Itinerary> genericRepository, IMapper mapper) : IItineraryService
    {
        public void AddItinerary(ItineraryDTO dto)
        {
            var itinerary = mapper.Map<Itinerary>(dto);
            genericRepository.Add(itinerary);
        }

        public void DeleteItinerary(int id)
        {
            var itinerary = genericRepository.GetById(id);
            genericRepository.Delete(itinerary);
        }

        public IEnumerable<ItineraryDTO> GetItineraries()
        {
            var itineraries = genericRepository.GetAll();
            var itinerariesDTO = mapper.Map<IEnumerable<ItineraryDTO>>(itineraries);
            return itinerariesDTO;
        }

        public ItineraryDTO GetItinerary(int id)
        {
            var itinerary = genericRepository.GetById(id);
            var itineraryDTO = mapper.Map<ItineraryDTO>(itinerary);
            return itineraryDTO;
        }

        public void Update(ItineraryDTO dto)
        {
            var itinerary = mapper.Map<Itinerary>(dto);
            genericRepository.Update(itinerary);
        }
    }
}
