﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs.Tour;
using System.Xml.Linq;

namespace BookTour.Library.Application.Serivce
{
    public class TourService(IMapper mapper, ITourRepository tourRepository) : ITourService
    {
        public void AddTour(TourForCreationDTO dto)
        {
            var Tour = mapper.Map<Tour>(dto);
            tourRepository.Add(Tour);
        }

        public void DeleteTour(int id)
        {
            var Tour = tourRepository.GetById(id);
            tourRepository.Delete(Tour);
        }

        public IEnumerable<TourDTO> GetTours()
        {
            string[] includes = ["Images"];
            var tours = tourRepository.GetAll(includes);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }

        public TourDetailsDTO GetTour(int id)
        {
            string[] includes = ["TourType","Transportation"];
            var Tour = tourRepository.GetById(id, includes);
            var TourDTO = mapper.Map<TourDetailsDTO>(Tour);
            return TourDTO;
        }

        public void Update(TourForCreationDTO dto)
        {
            var Tour = mapper.Map<Tour>(dto);
            tourRepository.Update(Tour);
        }

        public IEnumerable<TourDTO> SearchByName(string name)
        {
            var tours = tourRepository.SearchByName(name);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }

        public IEnumerable<TourDTO> SearchByDestination(string destination)
        {
            var tours = tourRepository.SearchByDestination(destination);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }

        public IEnumerable<TourDTO> SearchByStart(string start)
        {
            var tours = tourRepository.SearchByStart(start);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }

        public IEnumerable<TourDTO> SearchByDate(DateTime startDate)
        {
            var tours = tourRepository.SearchByDate(startDate);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }

        public IEnumerable<TourDTO> FilterByPrice(bool state)
        {
            var tours = tourRepository.FilterByPrice(state);
            var toursDTO = mapper.Map<IEnumerable<TourDTO>>(tours);
            return toursDTO;
        }
    }
}
