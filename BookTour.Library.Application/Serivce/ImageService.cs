﻿using AutoMapper;
using BookTour.Library.Application.IRepository;
using BookTour.Library.Application.IService;
using BookTour.Library.Domain.Entities;
using BookTour.Share.Model.DTOs;

namespace BookTour.Library.Application.Serivce
{
    public class ImageService(IImageRepository imageRepository, IMapper mapper) : IImageService
    {
        public void AddImage(ImageDTO dto)
        {
            var image = mapper.Map<Image>(dto);
            imageRepository.Add(image);
        }

        public void DeleteIImage(int id)
        {
            var image = imageRepository.GetById(id);
            imageRepository.Delete(image);
        }

        public ImageDTO GetImage(int id)
        {
            var image = imageRepository.GetById(id);
            var imageDTO = mapper.Map<ImageDTO>(image);
            return imageDTO;
        }

        public IEnumerable<ImageDTO> GetImages()
        {
            var images = imageRepository.GetAll();
            var imagesDTO = mapper.Map<IEnumerable<ImageDTO>>(images);
            return imagesDTO;
        }

        public IEnumerable<ImageDTO> GetImagesByTourId(int tourId)
        {
            var images = imageRepository.GetImagesByTourId(tourId);
            var imagesDTO = mapper.Map<IEnumerable<ImageDTO>>(images);
            return imagesDTO;
        }

        public void UpdateImage(ImageDTO dto)
        {
            var image = mapper.Map<Image>(dto);
            imageRepository.Update(image);
        }
    }
}
