﻿using BookTour.Library.Domain.Entities;

namespace BookTour.Library.Application.IRepository
{
    public interface IBookingRepository : IGenericRepository<Booking>
    {
        Booking Add(Booking booking);
    }
}
