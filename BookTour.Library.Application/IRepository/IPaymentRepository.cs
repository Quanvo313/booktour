﻿using BookTour.Library.Domain.Entities;

namespace BookTour.Library.Application.IRepository
{
    public interface IPaymentRepository : IGenericRepository<Payment>
    {
        IEnumerable<Payment> GetPaymentByUserId(string userId);
    }
}
