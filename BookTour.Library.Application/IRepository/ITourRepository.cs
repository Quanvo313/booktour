﻿using BookTour.Library.Domain.Entities;
using System;

namespace BookTour.Library.Application.IRepository
{
    public interface ITourRepository : IGenericRepository<Tour>
    {
        IEnumerable<Tour> SearchByName(string name);
        IEnumerable<Tour> SearchByDestination(string destination);
        IEnumerable<Tour> SearchByStart(string start);
        IEnumerable<Tour> SearchByDate(DateTime startDate);
        IEnumerable<Tour> FilterByPrice(bool state);

    }
}
