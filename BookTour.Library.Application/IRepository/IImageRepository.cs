﻿using BookTour.Library.Domain.Entities;

namespace BookTour.Library.Application.IRepository
{
    public interface IImageRepository : IGenericRepository<Image>
    {
        IEnumerable<Image> GetImagesByTourId(int tourId);
    }
}
