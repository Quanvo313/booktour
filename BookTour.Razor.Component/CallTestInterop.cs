﻿using Microsoft.JSInterop;

namespace BookTour.Razor.Component
{
    public static class CallTestInterop
    {
        public static ValueTask Test(this IJSRuntime jSRuntime)
        {
            return jSRuntime.InvokeVoidAsync("hello");
        }
    }
}
