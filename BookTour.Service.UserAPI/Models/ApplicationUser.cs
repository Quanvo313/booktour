﻿using Microsoft.AspNetCore.Identity;

namespace BookTour.Service.UserAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
    }
}
