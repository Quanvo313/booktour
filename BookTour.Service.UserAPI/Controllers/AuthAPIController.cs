﻿using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Mvc;
using BookTour.Service.UserAPI.Service.IService;
using Microsoft.AspNetCore.Authorization;

namespace BookTour.Service.Authentication.Controllers
{
    [ApiController]
    [Authorize]
    public class AuthAPIController(IUserService authService) : ControllerBase
    {
        protected ResponseDTO _response = new();
        [HttpPut("update-profile")]
        public async Task<IActionResult> UpdateProfile([FromBody] UpdateProfileRequestDTO model)
        {
            var message = await authService.UpdateUserProfile(model);
            if (string.IsNullOrEmpty(message))
            {
                message = "Error";
                return BadRequest(message);
            }
            return Ok(message);
        }

        [HttpGet("get-profile/{id}")]
        public async Task<IActionResult> GetProfile(string id)
        {
            var user = await authService.GetUserProfile(id);
            if (user == null)
            {
                return NotFound(user);
            }
            return Ok(user);
        }

    }
}
