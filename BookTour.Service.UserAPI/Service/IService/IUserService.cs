﻿using BookTour.Service.UserAPI.Models;
using BookTour.Share.Model.DTOs;

namespace BookTour.Service.UserAPI.Service.IService
{
    public interface IUserService
    {
        Task<string> UpdateUserProfile(UpdateProfileRequestDTO updateProfileRequestDTO);
        Task<UpdateProfileRequestDTO> GetUserProfile(string userId);
    }
}
