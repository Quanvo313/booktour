﻿using BookTour.Service.UserAPI.Data;
using BookTour.Service.UserAPI.Models;
using BookTour.Service.UserAPI.Service.IService;
using BookTour.Share.Model.DTOs;
using Microsoft.AspNetCore.Identity;

namespace BookTour.Service.UserAPI.Service
{
    public class UserService(
        AppDbContext db,
        UserManager<ApplicationUser> userManager,
        RoleManager<IdentityRole> roleManager) : IUserService
    {
        public async Task<string> UpdateUserProfile(UpdateProfileRequestDTO updateProfileRequestDTO)
        {
            var user = await userManager.FindByIdAsync(updateProfileRequestDTO.UserId);

            if (user == null)
            {
                return "";
            }

            user.FullName = updateProfileRequestDTO.Name;
            user.PhoneNumber = updateProfileRequestDTO.PhoneNumber;
            user.Email = updateProfileRequestDTO.Email;

            var result = await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return "Profile updated successfully";
            }
            else
            {
                return result.Errors.FirstOrDefault()?.Description ?? "Profile update failed";
            }
        }

        public async Task<UpdateProfileRequestDTO> GetUserProfile(string userId)
        {

            var user = await userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return null;
            }

            var userDto = new UpdateProfileRequestDTO
            {
                UserId = user.Id,
                Email = user.Email,
                Name = user.FullName,
                PhoneNumber = user.PhoneNumber,
            };

            return userDto;
        }
    }
}
