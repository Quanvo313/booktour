﻿namespace BookTour.Library.Domain.Entities
{
    public class ItineraryDays
    {
        public int ItineraryDaysId { get; set; }
        public string Name { get; set; }
        public int ItineraryId { get; set; }
        public Itinerary Itinerary { get; set; }
        public List<ItineraryDetail> ItineraryDetails { get; set; }
    }
}