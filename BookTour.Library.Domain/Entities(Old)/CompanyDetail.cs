﻿namespace BookTour.Library.Domain.Entities
{
    public class CompanyDetail
    {
        public int CompanyDetailId { get; set; }
        public string CompanyDetailType { get; set; }
        public string CompanyDetailDetail { get; set; }
    }
}
