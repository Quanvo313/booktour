﻿namespace BookTour.Library.Domain.Entities
{
    public class Itinerary
    {
        public int ItineraryId { get; set; }
        public int NumberOfDays { get; set; }
        public int TourId { get; set; }

        public Tour Tour { get; set; }
        public List<ItineraryDays> ItineraryDays { get; set; }
    }
}
