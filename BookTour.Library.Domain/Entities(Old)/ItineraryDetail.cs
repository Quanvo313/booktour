﻿namespace BookTour.Library.Domain.Entities
{
    public class ItineraryDetail
    {
        public int ItineraryDetailId { get; set; }
        public string? ItineraryDetailName { get; set; }
        public string? ItineraryDetailText { get; set; }
        public int ItineraryDaysId { get; set; }

        public ItineraryDays ItineraryDays { get; set; }
    }
}
