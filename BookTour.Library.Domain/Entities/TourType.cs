﻿namespace BookTour.Library.Domain.Entities
{
    public class TourType
    {
        public int TourTypeId { get; set; }
        public string TourTypeName { get; set; }
    }
}
