﻿namespace BookTour.Library.Domain.Entities
{
    public class Booking
    {
        public int BookingId { get; set; }
        public int TourId { get; set; }
        public string UserId { get; set; }

        public int NumberOfPeople { get; set; }
        public DateTime BookingDate { get; set; } = DateTime.Now;

        public decimal TotalCost { get; set; } = decimal.Zero;
        public Tour Tour { get; set; }
    }
}
