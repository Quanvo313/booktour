﻿namespace BookTour.Library.Domain.Entities
{
    public class Image
    {
        public int ImageId { get; set; }
        public int TourId { get; set; }
        public string ImageUrl { get; set; }
        public string ImageText { get; set; }
        public bool IsMain { get; set; } = false;
        public Tour Tour { get; set; }
    }
}
