﻿namespace BookTour.Library.Domain.Entities
{
    public class Transportation
    {
        public int TransportationId { get; set; }
        public string TransportationName { get; set; }
    }
}
