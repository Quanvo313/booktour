﻿namespace BookTour.Library.Domain.Entities
{
    public class Tour
    {
        public int TourId { get; set; }
        public string TourName { get; set; }
        public string Start { get; set; }
        public int TourTypeId { get; set; }
        public string Description { get; set; }
        public decimal PricePerPerson { get; set; }
        public int TourDuration { get; set; }
        public int TransportationId { get; set; }
        public string Destination { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        //navigation
        public Transportation Transportation { get; set; }
        public ICollection<Booking> Bookings { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public TourType TourType { get; set; }

    }
}
