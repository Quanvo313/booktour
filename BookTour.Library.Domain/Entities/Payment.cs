﻿namespace BookTour.Library.Domain.Entities
{
    public class Payment
    {
        public int PaymentID { get; set; }
        public int BookingID { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Amount { get; set; }
        public bool IsCompleted { get; set; } = false;
        // Navigation property
        public Booking Booking { get; set; }
    }
}
